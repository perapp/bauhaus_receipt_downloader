from collections import namedtuple
from dataclasses import dataclass
import dataclasses
import datetime
import locale
import browser_cookie3
import requests
import pathlib
import openpyxl
from openpyxl.cell.cell import Cell
from bs4 import BeautifulSoup
import urllib.parse
import dateutil.parser

class Store(object):
    def __init__(self) -> None:
        self.receipts = {}
        self.cj = browser_cookie3.load()

    def fetch_receipts(self):
        for receipt_id in self.fetch_receipt_ids():
            self.receipts[receipt_id] = self.fetch_receipt(receipt_id)
    
    def fetch_soup(self, url):
        data = self.fetch_url(url)
        return BeautifulSoup(data, 'html.parser')

    def fetch_url(self, url, **kwargs) -> requests.Response:
        cache_path = pathlib.Path.cwd()/"cache"
        for x in url.replace("https://", "").split("/"):
            cache_path = cache_path/x.replace("?","")
        cache_path = cache_path.parent/f"{cache_path.name}.dat"

        if cache_path.exists():
            print(url, "(cache)")
            return cache_path.read_bytes()
        
        resp = requests.get(url, cookies=self.cj, **kwargs)
        print(url, f"(http {resp.status_code})")
        if resp.status_code == 200:
            cache_path.parent.mkdir(parents=True, exist_ok=True)
            cache_path.write_bytes(resp.content)
        return resp.content
    
    def save_excel(self):
        wbfile = pathlib.Path("receipts.xlsx")
        if wbfile.exists():
            wb = openpyxl.load_workbook(filename = wbfile)
        else:
            wb = openpyxl.Workbook()

        sheetname = "data"
        #if sheetname in wb.sheetnames:
        #    wb.remove(wb[sheetname])
        if sheetname not in wb.sheetnames:
            ws = wb.create_sheet(sheetname)
            ws.append(["Store", "Receipt URL", "Receipt Id", "Purchase Time", "Part Id", "Part Descr", "Amount", "Price", "Cost"])
        else:
            ws = wb[sheetname]

        existing_urls = set(list(ws.iter_cols(min_col=2, max_col=2, min_row=2, values_only=True))[0])
        for rec in self.receipts.values():
            if rec.url not in existing_urls:
                print("add", rec.url)
                for row in rec.rows:
                    ws.append([rec.store,
                            rec.url,
                            rec.id,
                            rec.time,
                            row.part_id,
                            row.part_description,
                            row.amount,
                            row.price,
                            row.cost])
        wb.save(wbfile)

class Bauhaus(Store):
    def fetch_receipt_ids(self):
        soup = self.fetch_soup("https://mysitebauhaus.stroederalton.se/Receipt")
        for receipt in soup.find("section", class_="receiptList").find_all("a"):
            if "/ReceiptDetail/" in receipt["href"]:
                yield receipt["href"].split("/ReceiptDetail/")[1]

    def fetch_receipt(self, receipt_id):
        locale.setlocale(locale.LC_ALL, 'sv_SE.UTF-8')
        url = f"https://mysitebauhaus.stroederalton.se/Member/ReceiptDetail/{receipt_id}"
        soup = self.fetch_soup(url)
        pathlib.Path("x.html").write_text(soup.prettify())

        time_str = list(soup.find("h2", string="KVITTODETALJER").next_siblings)[3].get_text()
        rec = Receipt(url=url,
                      id=receipt_id,
                      time=dateutil.parser.parse(time_str),
                      store="Bauhaus")
        for row in soup.find("tbody").find_all("tr", recursive=False):
            row_attrs = [x.get_text().strip() for x in row.find_all("td", recrsive=False)]
            price_with_currency = row_attrs[2]
            price_text = price_with_currency.split(" ")[0]
            price = locale.atof(price_text)
            rec.rows.append(ReceiptRow(rec,
                                       part_id=row_attrs[0],
                                       part_description=row_attrs[1],
                                       price=price,
                                       amount=1,
                                       cost=price))
        return rec

class Biltema(Store):

    def fetch_receipt_ids(self):
        soup = self.fetch_soup("https://www.biltema.se/mitt-biltema/mina-kvitton/")
        for receipt in soup.find_all("a", class_="js__ga__track--mybiltema--clicked-show-receipt"):
            qs = urllib.parse.urlparse(receipt["href"]).query
            yield urllib.parse.parse_qs(qs)["receiptId"][0]

    def fetch_receipt(self, receipt_id):
        locale.setlocale(locale.LC_ALL, 'sv_SE.UTF-8')
        url = f"https://www.biltema.se/mitt-biltema/mina-kvitton/receipt/?receiptId={receipt_id}"
        soup = self.fetch_soup(url)
        pathlib.Path("x.html").write_text(soup.prettify())

        receipt_header = soup.find("ul", class_="receipt__header--row")
        attrs = [x.get_text().strip() for x in receipt_header.find_all("li", recursive=False)]
        rec = Receipt(url=url,
                      id=attrs[0],
                      time=dateutil.parser.parse(attrs[1]),
                      store=attrs[2])
        for row in soup.find_all("ul", class_="receipt__body--row"):
            row_attrs = []
            for x in row.find_all("li", recursive=False):
                ss = list(x.stripped_strings)
                row_attrs.append(ss[1] if len(ss)>1 else ss[0])
            rec.rows.append(ReceiptRow(rec,
                                       part_id=row_attrs[0],
                                       amount=locale.atof(row_attrs[1]),
                                       part_description=row_attrs[2],
                                       price=locale.atof(row_attrs[3]),
                                       cost=locale.atof(row_attrs[4])))
        return rec

class KRauta(Store):
    """
    Fails with HTTP 403 forbidden even if cookies loaded.

    https://www.k-rauta.se/mina-sidor/start
        window.__PRELOADED_STATE__
    https://www.k-rauta.se/api/myPagesSe/contactOverview/per@appgren.se
    https://www.k-rauta.se/api/myPagesSe/transactions/17540468-a59f-4b6b-9a32-a96600c01f83

    """
    def fetch_receipt_ids(self):
        #data = self.fetch_url("https://www.k-rauta.se/api/myPagesSe/transactions/17540468-a59f-4b6b-9a32-a96600c01f83").json()
        #soup = self.fetch_soup("https://www.k-rauta.se/api/myPagesSe/contactOverview/per@appgren.se")
        #for x in soup.stripped_strings:
        #    print(x)
        return []

@dataclass
class Receipt(object):
    url: str
    store: str
    id: str
    time: datetime.datetime
    rows: list = dataclasses.field(default_factory=list)

@dataclass
class ReceiptRow:
    receipt: Receipt
    part_id: str
    part_description: str
    amount: int = None
    price: float = None
    cost: float = None



def todo():
    wbfile = pathlib.Path("bauhaus.xlsx")
    if wbfile.exists():
        wb = openpyxl.load_workbook(filename = wbfile)
    else:
        wb = openpyxl.Workbook()

    sheetname = "data"
    if sheetname in wb.sheetnames:
        wb.remove(wb[sheetname])
    ws = wb.create_sheet(sheetname)
    ws.append(["time", "amount"])
    

for Store in Bauhaus, Biltema:
    store = Store()
    store.fetch_receipts()
    store.save_excel()

