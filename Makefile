PY:=.venv/bin/python

download: .venv
	$(PY) -m bauhaus_receipt_downloader.main

shell: .venv
	. .venv/bin/activate; bash

.venv: requirements.txt
	python3 -m venv .venv
	$(PY) -m pip install --upgrade pip
	$(PY) -m pip install -r requirements.txt
	touch .venv

clean:
	rm -rf .venv


